"use strict";

var express = require('express');

var user = require('./src/services/userService.js');

var app = express();
app.get('/', function (request, response) {
  response.send('something return to client');
});
app.post('/users', function (request, response) {
  user.loadUser('./src/data/users.json').then(function (users) {
    response.json({
      success: true,
      data: users
    });
  })["catch"](function (e) {
    response.json({
      success: false,
      message: "Users can't be found!"
    });
  });
});
app.listen(3000, function () {
  console.log('Server has just been started');
});
//# sourceMappingURL=index.js.map