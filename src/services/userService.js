const fs = require('fs');

function loadUser(path) {

    return new Promise(function (resolve, reject) {
        try {
            resolve(JSON.parse(fs.readFileSync(path, 'utf8')));
        } catch (e) {
            reject(e);
        }
    });

}


function createUser(user) {
    return {
        ...user
    }
}

exports.createUser = createUser;
exports.loadUser = loadUser;