import dotenv from 'dotenv';

const environment = dotenv.config();
if (!environment) {
    throw new Error('Config file was not found!');
}
export default {
    PORT: process.env.PORT || 3005,

    PREFIX: process.env.PREFIX || '/api',

    VERSION: process.env.VERSION || '1.0.0'
}