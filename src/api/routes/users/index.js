import {Router} from 'express';

const route = Router();

export default mainRoute => {
    mainRoute.use('/users', route);

    route.get('/', (request, response, next) => {
        console.log('Hello from first handler!')
        response.json({
            success: true,
            data: []
        })
    })

    route.post('/', (request, response, next) => {

    })

    route.put('/:id', (request, response, next) => {

    })
}