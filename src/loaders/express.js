import route from '../api'
import bodyParser from 'body-parser';
import cors from 'cors';

const allowedOrigins = [
    'http://localhost:3000',
]

export default async ({app, config}) => {
    app.use(`${config.PREFIX}/v${config.VERSION}`, route());

    app.use(cors({
        origin: function (origin, callback) {
            // allow requests with no origin
            // (like mobile apps or curl requests)
            if (!origin) {
                return callback(null, true);
            }
            if (allowedOrigins.indexOf(origin) === -1) {
                const msg = 'The CORS policy for this site does not allow access from the specified Origin.';
                return callback(new Error(msg), false);
            }
            return callback(null, true);
        },
        credentials: true
    }));

    app.use(bodyParser.json());
}