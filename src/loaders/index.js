import expressLoader from './express';

export default async ({app, config}) => {


    await expressLoader({app, config});
    console.log('Express loaded');
}